package com.enuke.videostream.youtube;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.enuke.videostream.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener,
        IPlayVideo, YouTubePlayer.OnFullscreenListener, YouTubePlayer.PlaylistEventListener{

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    RecyclerView recycler;
    VideoListAdapter adapter;
    ArrayList<VideoEntry> videoList;
    String videoId;
    static int videoNumber;
    YouTubePlayer youTubePlayer;
    private Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
    private ThumbnailListener thumbnailListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);

        setVideoList();
        thumbnailListener = new ThumbnailListener();
        thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(layoutManager);

        if(videoList == null) {
            videoList = new ArrayList<>();
        }
        adapter = new VideoListAdapter(this, videoList,
                this,thumbnailListener, thumbnailViewToLoaderMap);
        recycler.setAdapter(adapter);
    }

    private void setVideoList() {
        ArrayList<VideoEntry> list = new ArrayList<VideoEntry>();
        list.add(new VideoEntry(0,"YouTube Collection", "Y_UmWdcTrrc"));
        list.add(new VideoEntry(1,"GMail Tap", "1KhZKNZO8mQ"));
        list.add(new VideoEntry(2,"Chrome Multitask", "UiLSiqyDf4Y"));
        list.add(new VideoEntry(3,"Google Fiber", "re0VRK6ouwI"));
        list.add(new VideoEntry(4,"Autocompleter", "blB_X38YSxQ"));
        list.add(new VideoEntry(5,"GMail Motion", "Bu927_ul_X0"));
        list.add(new VideoEntry(6,"Translate for Animals", "3I24bSteJpw"));
        videoList = list;// (ArrayList<VideoEntry>) Collections.unmodifiableList(list);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer = player;
            youTubePlayer.cueVideo("fhWaJi1Hsfo");
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(DeveloperKey.DEVELOPER_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Override
    public void play(VideoEntry entry) {
        if (entry.getVideoId() != null && !entry.getVideoId().equals(this.videoId)) {
            this.videoId = entry.getVideoId();
            if (youTubePlayer != null) {
                youTubePlayer.loadVideo(videoId);
                youTubePlayer.play();
                //youTubePlayer.cueVideo(videoId);
                videoNumber = entry.getNum();
            }
        }
    }

    @Override
    public void onFullscreen(boolean b) {
        if(b){
            releaseLoaders();
        }
    }

    @Override
    public void onPrevious() {
        if(videoNumber != 0){
            play(videoList.get(videoNumber-1));
        }
    }

    @Override
    public void onNext() {
        if(videoNumber != videoList.size()-1){
            play(videoList.get(videoNumber+1));
        }
    }

    @Override
    public void onPlaylistEnded() {

    }

    public final class ThumbnailListener implements
            YouTubeThumbnailView.OnInitializedListener,
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onInitializationSuccess(YouTubeThumbnailView view,
                                            YouTubeThumbnailLoader loader) {
            loader.setOnThumbnailLoadedListener(this);
            thumbnailViewToLoaderMap.put(view, loader);
            view.setImageResource(R.drawable.loading_thumbnail);
            String videoId = (String) view.getTag();
            loader.setVideo(videoId);
        }

        @Override
        public void onInitializationFailure(
                YouTubeThumbnailView view, YouTubeInitializationResult loader) {
            view.setImageResource(R.drawable.no_thumbnail);
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
            view.setImageResource(R.drawable.no_thumbnail);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (youTubePlayer != null) {
            youTubePlayer.release();
        }
    }

    public void releaseLoaders() {
        for (YouTubeThumbnailLoader loader : thumbnailViewToLoaderMap.values()) {
            loader.release();
        }
    }
}
