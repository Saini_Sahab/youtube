package com.enuke.videostream.youtube;

/**
 * Created by enuke on 11/16/17.
 */

public interface IPlayVideo {
    void play(VideoEntry entry);
}
