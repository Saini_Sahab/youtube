package com.enuke.videostream.youtube;

/**
 * Created by enuke on 11/16/17.
 */

public class VideoEntry {
    private int num;
    private String text;
    private String videoId;

    public VideoEntry(int num,String text, String videoId) {
        this.text = text;
        this.videoId = videoId;
        this.num = num;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
