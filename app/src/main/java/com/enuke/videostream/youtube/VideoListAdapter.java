package com.enuke.videostream.youtube;

import android.content.Context;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enuke.videostream.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 11/16/17.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoHolder> {

    public IPlayVideo playVideo;
    Context context;
    ArrayList<VideoEntry> videoList;
    YouTubeActivity.ThumbnailListener thumbnailListener;
    Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;


    public VideoListAdapter(Context context, ArrayList<VideoEntry> videoList,
                            IPlayVideo playVideo,
                            YouTubeActivity.ThumbnailListener thumbnailListener,
                            Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap) {
        this.context = context;
        this.videoList = videoList;
        this.playVideo = playVideo;
        this.thumbnailListener = thumbnailListener;
        this.thumbnailViewToLoaderMap = thumbnailViewToLoaderMap;
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_item, null);
        return new VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        final VideoEntry current = videoList.get(position);

        if (holder.thumbnailView.getTag() == null) {
            holder.thumbnailView.setTag(current.getVideoId());
            holder.thumbnailView.initialize(DeveloperKey.DEVELOPER_KEY, thumbnailListener);
        } else {
            YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(holder.thumbnailView);
            if (loader == null) {
                // 2) The view is already created, and is currently being initialized. We store the
                //    current videoId in the tag.
                holder.thumbnailView.setTag(current.getVideoId());
            } else {
                // 3) The view is already created and already initialized. Simply set the right videoId
                //    on the loader.
                holder.thumbnailView.setImageResource(R.drawable.loading_thumbnail);
                loader.setVideo(current.getVideoId());
            }
        }

        holder.textView.setText(current.getText());
        // holder.textView.setVisibility(labelsVisible ? View.VISIBLE : View.GONE);

        holder.videoItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playVideo.play(current);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        YouTubeThumbnailView thumbnailView;
        TextView textView;
        RelativeLayout videoItem;

        public VideoHolder(View itemView) {
            super(itemView);
            thumbnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.thumbnail);
            textView = (TextView) itemView.findViewById(R.id.text);
            videoItem = (RelativeLayout) itemView.findViewById(R.id.videoItem);
        }
    }


}
